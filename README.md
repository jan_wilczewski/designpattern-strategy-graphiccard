Celem tego zadania nie jest skupianie się na szczegółach działania (implementacji metod), tylko na własnej implementacji wzorca projektowego.

Stwórz aplikację symulującą sterownik karty graficznej. Stworzymy obiekt karty graficznej która zdolna jest do wyświetlania obrazu. Format obrazu ma kilka standardów - podstawowe dwa parametry to rozdzielczość i gama kolorów (32 bit, 16 bit, mono, 256 kolorów). oraz format (16:10, 16:9, 4:3, 3:2).

Stwórz klasę GraphicsCard która reprezentuje kartę graficzną. Strategia w tym projekcie będzie służyć do podmieniania w tej klasie obiektu sterownika. Stwórz interfejs sterownika/ustawień karty graficznej. Mechanizm strategii powinien być wykorzystany do podmiany w klasie GraphicsCard obiektu sterownika/ustawienia karty graficznej. 

Sterownik powinien posiadać metodę:
•	getNeededProcessingPower która wylicza ile mocy powinna mieć karta graficzna aby udźwignąć tą kartę graficzną. 
•	processFrame która jako parametr przyjmuje ramkę (tablica dwuwymiarowa int'ów) i w zależności od typu sterownika (klasy dziedziczącej) wypełnia tablicę innymi wartościami.

Podane powyżej dane powinny wystarczyć do stworzenia modelu w oparciu o wzorzec projektowy strategia. Poniżej znajdują się wskazówki do wykonania tego zadania.

1.) Stwórz klasę GraphicsCard.
2.) Stwórz klasę GraphicsSetting
3.) Stwórz w GraphicsCard pole typu GraphicsSetting (strategia).
4.) Stwórz 3 klasy dziedziczące: HDSetting, MediumSetting, LowSetting które dziedziczą po GraphicsSetting (strategia)
5.) Dodaj do klasy GraphicsSetting oraz klas dziedziczących implementację metod getNeededProcessingPower i processFrame.
6.) Stwórz implementację testową (main) która pozwala przestawiać podmianę klas i zasadę działania. 
7.) Stwórz dodatkowe ustawienie (własne)
