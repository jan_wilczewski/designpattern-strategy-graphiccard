import graphicSettings.IGraphicSettings;

/**
 * Created by jan_w on 04.10.2017.
 */
public class GraphicCard {

    private IGraphicSettings settings;

    public void ustawKonfiguracje(IGraphicSettings konfiguracja){
        this.settings = konfiguracja;
    }

    public void uzyjUstawienia(){
        Integer[] tab = new Integer[5];
        settings.processFrame(tab);
    }
}
