package graphicSettings;

/**
 * Created by jan_w on 04.10.2017.
 */
public class MediumSettings implements IGraphicSettings {

    @Override
    public double getNeededProcessingPower() {
        return 2;
    }

    @Override
    public void processFrame(Integer[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = 2;
        }
        System.out.println("MediumSettings started");
    }
}
