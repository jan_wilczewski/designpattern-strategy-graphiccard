package graphicSettings;

/**
 * Created by jan_w on 04.10.2017.
 */
public interface IGraphicSettings {

    public double getNeededProcessingPower();
    public void processFrame(Integer[] tablica);

}
