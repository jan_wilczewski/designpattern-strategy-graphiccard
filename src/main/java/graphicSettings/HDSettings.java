package graphicSettings;

/**
 * Created by jan_w on 04.10.2017.
 */
public class HDSettings implements IGraphicSettings{

    @Override
    public double getNeededProcessingPower() {
        return 3;
    }

    @Override
    public void processFrame(Integer[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = 3;
        }
        System.out.println("HDSettings started");
    }
}
