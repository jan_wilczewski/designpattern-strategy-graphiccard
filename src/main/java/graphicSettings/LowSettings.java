package graphicSettings;

/**
 * Created by jan_w on 04.10.2017.
 */
public class LowSettings implements IGraphicSettings{

    public LowSettings() {
    }

    @Override
    public double getNeededProcessingPower() {
        return 1;
    }

    @Override
    public void processFrame(Integer[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = 1;
        }
        System.out.println("LowSettings started");
    }
}
