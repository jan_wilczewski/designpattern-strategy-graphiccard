import graphicSettings.HDSettings;
import graphicSettings.LowSettings;
import graphicSettings.MediumSettings;

import java.util.Scanner;

/**
 * Created by jan_w on 04.10.2017.
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        GraphicCard karta = new GraphicCard();
        System.out.println("Podaj ustawienia");

        while (scanner.hasNextLine()) {

            String ustawienia = scanner.next();

            if (ustawienia.equals("low")) {
                karta.ustawKonfiguracje(new LowSettings());
            } else if (ustawienia.equals("medium")) {
                karta.ustawKonfiguracje(new MediumSettings());
            } else if (ustawienia.equals("hd")) {
                karta.ustawKonfiguracje(new HDSettings());
            }
            else if (ustawienia.equals("uzyj")){
                karta.uzyjUstawienia();
            }
        }
    }
}
